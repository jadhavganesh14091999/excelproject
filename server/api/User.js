const express = require("express");
const router = express.Router();

const User = require("../models/User");

router.post("/", async (req, res) => {
  const userData = await req.body;
  try {
    let user = await User.insertMany(userData);
    res.status(200).send({ msg: "Data Inserted Successfully" });
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
