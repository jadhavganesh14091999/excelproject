const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
  },

  vul_type: {
    type: String,
  },

  summary: {
    type: String,
  },

  tech_details: {
    type: String,
  },

  remediation: {
    type: String,
  },

  req: {
    type: String,
  },

  res: {
    type: String,
  },

  cvss_score: {
    type: Number,
  },

  cvss_eq: {
    type: String,
  },

  severity: {
    type: String,
  },
});

module.exports = User = mongoose.model("users", UserSchema);
