const express = require("express");
const path = require("path");

const connectDB = require("./db.js");
const cors = require("cors");
const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

connectDB();
// Import Database Model

const app = express();

// Init Middleware
app.use(cors(corsOptions));
app.use(express.json({ extended: false }));
app.use("/users", require("./api/User"));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () =>
  console.log(`Server started on port http://localhost:${PORT}`)
);
