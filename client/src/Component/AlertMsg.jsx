import { Alert } from "@mui/material";
import React from "react";

const AlertMsg = (props) => {
  return <Alert>{props.msg}</Alert>;
};

export default AlertMsg;
