import {
  Button,
  Box,
  Typography,
  Modal,
  TableHead,
  Table,
  TableRow,
  TableCell,
  TableBody,
} from "@mui/material";
import React, { Fragment, useState } from "react";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  maxWidth: 800,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  //   overflow: "auto",
};

const ExpandRow = (props) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}>{!open ? "Expand" : "Hide"}</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Request & Response
          </Typography>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Request</TableCell>
                <TableRow>
                  <TableCell align="center">
                    <div
                      style={{
                        height: "100px",
                        width: "700px",
                        overflow: "auto",
                      }}
                    >
                      {props.req}
                    </div>
                  </TableCell>
                </TableRow>
              </TableRow>
              <TableRow>
                <TableCell>Response</TableCell>
                <TableRow>
                  <TableCell align="center">
                    <div
                      style={{
                        height: "100px",
                        width: "700px",
                        overflow: "auto",
                      }}
                    >
                      {props.res}
                    </div>
                  </TableCell>
                </TableRow>
              </TableRow>
            </TableHead>
          </Table>
        </Box>
      </Modal>
    </div>
    // <div>
    //   <Button variant="contained" onClick={handleToggle}>
    //     {!toggle ? "Expand" : "Hide"}
    //   </Button>
    //   {toggle && (
    //     <div style={{ display: "flex", justifyContent: "space-between" }}>
    //       <div style={{ height: "70px", width: "250px", overflow: "auto" }}>
    //         <p>req:{props.req} </p>
    //       </div>
    //       <div style={{ height: "70px", width: "250px", overflow: "auto" }}>
    //         <p>req:{props.res} </p>
    //       </div>
    //     </div>
    //   )}
    // </div>
  );
};

export default ExpandRow;
