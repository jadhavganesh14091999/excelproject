import React, { useState, useEffect, Fragment } from "react";
import classes from "../CSS/DataUpload.module.css";
import * as XLSX from "xlsx";
import axios from "axios";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, Alert, Tab } from "@mui/material";
import AlertMsg from "./AlertMsg";
import ExpandRow from "./ExpandRow";

const DataUpload = () => {
  const [initialSend, setInitialSend] = useState(true);
  const [sendData, setSendData] = useState([]);
  const [addData, setAddData] = useState(false);
  const [display, setDisplay] = useState(false);
  const [notification, setNotification] = useState();

  useEffect(() => {
    // console.log(sendData);
    // let payload = JSON.parse(sendData);
    // console.log("type of ", typeof payload);

    const excelData = async () => {
      // console.log(se)
      try {
        console.log(sendData);
        const url = "http://localhost:5000/users";
        const config = {
          "Content-Type": "application/json",
        };
        const response = await axios.post(url, sendData, config);
        console.log(response);
        const msg = response.data.msg;
        console.log(msg);
        setNotification(msg);
      } catch (error) {
        console.log(error);
      }
    };

    if (!initialSend) {
      setDisplay(true);
      excelData();
    } else {
      setInitialSend(false);
    }
  }, [addData]);

  const onChange = (e) => {
    //
    const [file] = e.target.files;
    const reader = new FileReader();

    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: "binary" });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];

      const data = XLSX.utils.sheet_to_json(ws, {
        header: [
          "no",
          "name",
          "vuln_type",
          "summary",
          "tech_details",
          "remediation",
          "req",
          "res",
          "cvss_score",
          "cvss_eq",
          "severity",
        ],
        blankrows: false,
      });
      data.shift();
      console.log(data);
      setSendData(data);
      setNotification("");
    };
    reader.readAsBinaryString(file);
  };

  const addDataHandler = () => {
    setAddData(true);
  };
  const removeNotification = () => {
    setDisplay(false);
  };
  return (
    <Fragment>
      {display && (
        <div
          style={{
            marginTop: 20,
            marginLeft: "100px",
            marginRight: "100px",
          }}
        >
          {notification !== "" && (
            <div>
              <AlertMsg msg={notification} />

              {/* <button
                style={{
                  float: "right",
                  color: "white",
                }}
                onClick={removeNotification}
              >
                X
              </button> */}
            </div>
          )}
        </div>
      )}

      <div className={classes.excelupload}>
        <p>Choose Excel File to Upload</p>
        <input type="file" id="input" onChange={onChange}></input>
      </div>
      <div style={{ margin: "50px" }}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow sx={{ fontSize: 20 }}>
                <TableCell>No.</TableCell>
                <TableCell>Name</TableCell>
                <TableCell align="right">Vuln Type</TableCell>
                <TableCell align="right">Summary</TableCell>
                <TableCell align="right">Tech Details</TableCell>
                <TableCell align="right">Remediation</TableCell>
                <TableCell align="center">req,res</TableCell>
                <TableCell align="right">cvss score</TableCell>
                <TableCell align="right">cvsseq</TableCell>
                <TableCell align="right">severity</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sendData.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.no}
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.vuln_type}</TableCell>
                  <TableCell align="right">{row.summary}</TableCell>
                  <TableCell align="right">{row.tech_details}</TableCell>
                  <TableCell align="right">{row.remediation}</TableCell>
                  <TableCell align="center">
                    <ExpandRow req={row.req} res={row.res} />
                  </TableCell>
                  {/* <TableCell align="right">{row.req}</TableCell>
                  <TableCell align="right">{row.res}</TableCell> */}
                  <TableCell align="right">{row.cvss_score}</TableCell>
                  <TableCell align="right">{row.cvss_eq}</TableCell>
                  <TableCell align="right">{row.severity}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        {/* {addData && ( */}
        <div style={{ textAlign: "center" }}>
          <Button onClick={addDataHandler}>Add Data</Button>
        </div>
        {/*  */}
        {/* {!addData && (
          <div style={{ textAlign: "center", marginTop: 10 }}>
            No file Selected or File is Empty
          </div>
        )} */}
      </div>
    </Fragment>
  );
};

export default DataUpload;
