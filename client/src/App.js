import logo from "./logo.svg";
import "./App.css";
import DataUpload from "./Component/DataUpload";

function App() {
  return (
    <div>
      <DataUpload />
    </div>
  );
}

export default App;
